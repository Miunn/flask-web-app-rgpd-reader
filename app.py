import re

from flask import Flask, render_template, request, redirect, url_for
from db import *

# Création de l'application web Flask
app = Flask(__name__)


""" Définition des endpoints d'écoute """


@app.route("/admin", methods=['GET', 'POST'])
def admin():
    d = [""]*9
    if request.method == 'POST':
        try:
            d = request.args['data'].split(';')
        except:
            pass
    return render_template("index.html", data=d)


@app.route('/admin/fetchadmin', methods=['POST'])
def fetch_admin():
    fetched = fetch_data(request.form.get('id'))
    return redirect(url_for('admin', data=f"{fetched['firstname']};{fetched['lastname']};{fetched['birthdate']};{fetched['birthplace']};{fetched['degreelvl']};{fetched['postaladdress']};{fetched['mail']};{fetched['ssn']};{fetched['phonenumber']}"), code=307)


if __name__ == "__main__":
    app.run(host='127.0.0.1', port=5000, debug=True)
