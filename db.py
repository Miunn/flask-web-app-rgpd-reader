# coding: utf-8

import mariadb


def get_db_params() -> dict:
    return {
        "host": "127.0.0.1",
        "port": 3306,
        "user": "appWebReader",
        "password": "mariadbReader321!",
        "database": "TestopEmbauche"
    }


def fetch_data(id_: str) -> dict:
    query = f"select * from submits where id={id_}"
    with mariadb.connect(**get_db_params()) as db:
        with db.cursor() as crsr:
            crsr.execute(query)
            ks = ("id", "firstname", "lastname", "birthdate", "birthplace",
                  "degreelvl", "postaladdress", "mail", "ssn", "phonenumber", "created")
            rs = crsr.fetchall()
            print('Fetch successfull')
            return {k: v for k, v in zip(ks, rs[0])}
